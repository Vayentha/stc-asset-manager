import React from "react";

export function test_library()
{
    return (
        <div>
            test
        </div>
    );
}


export async function hit_api_feed(url)
{
    let response = await fetch(url);


    if ( response.ok)
    {
        // if HTTP-status is 200-299
        // get the response body (the method explained below)'
         let t = await response.json();
         return t;
    }
    else
    {
        alert("HTTP-Error: " + response.status);
    }
}


