import React from 'react';
import {Redirect, BrowserRouter, Route, Router} from 'react-router-dom';
import HeartButton from './components/like'
import StockTable from "./components/stockTable";
import OwnedStocksTable from "./components/ownedStocksTable";
import DevBanner from "./components/devbanner";
import Toggle from "./components/toggleButton";
import SubscriptionForm from "./components/SubscriptionForm";
import {Jumbotron} from "react-bootstrap";
import {Container} from "react-bootstrap";
import ScrollMenu from "react-horizontal-scrolling-menu";
import {test_library, hit_api_feed} from "./InternalLib";
import {Alert} from "react-bootstrap";



import Button from "@material-ui/core/Button";
import './App.css';
import AssetCard from "./components/displays/AssetCard";
import BasicAccordion from "./components/displays/BasicAccordion";
import LoginField from "./components/inputs/LoginField";
import {Card} from "@material-ui/core";
import {Box} from "@material-ui/core";
import {Typography} from "@material-ui/core";
import 'fontsource-roboto';


const fetch = require("node-fetch");


// list of items
//TODO build this from actual objects
const list = [
    { name: 'item1' },
    { name: 'item2' },
    { name: 'item3' },
    { name: 'item4' },
    { name: 'item5' },
    { name: 'item6' },
    { name: 'item7' },
    { name: 'item8' },
    { name: 'item9' }
];

// One item component
// selected prop will be passed
const MenuItem = ({text, selected}) => {
    return <div
        className={`menu-item ${selected ? 'active' : ''}`}
    >
    <AssetCard/>
    </div>;
};

// All items component
// Important! add unique key
export const Menu = (list, selected) =>
    list.map(el => {
        const {name} = el;

        return <MenuItem text={name} key={name} selected={selected} />;
    });

// makes the arrow for the scrolling menu
const Arrow = ({ text, className }) => {
    return (
        <div
            className={className}
        >{text}</div>
    );
};

//left/right arrow definitions here
const ArrowLeft = Arrow({ text: '<', className: 'arrow-prev' });
const ArrowRight = Arrow({ text: '>', className: 'arrow-next' });

const selected = 'item1';







class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            host:"127.0.0.1",
            dbh: "10.0.0.21",
            api_port: 6543,
            message: [],
            selected: selected,
            feed: "",
            wex: "",
            redirect: false,
        });
        this.onSubmitMessage = this.onSubmitMessage.bind(this);
        this.menuItems = Menu(list, selected);
    }


    onSelect = key => {
        this.setState({ selected: key });
    };


    refreshPage() {
        window.location.reload(false);
    }



    //this will get the props from the form
    onSubmitMessage(message) {
        this.setState({ message: message });
        //console.log(this.state);
    }

    //set the redirect to other pages (defined in Routes.js) here
    //page name is set in paget (page type) variable
    setRedirect(paget){
        this.setState({
            redirect: true,
            page: paget
        })
    }

    renderRedirect(){
        if (this.state.redirect) {
            if(this.state.page === 'vendors') {
                return <Redirect to={'/vendors'}/>
            }
            if(this.state.page === 'graphs') {
                return <Redirect to={'/graphs/'}/>
            }
            if(this.state.page === 'realestate') {
                return <Redirect to={'/realestate/'}/>
            }
        }
    }

    fetchapi = async () => {
        const response = await fetch('http://127.0.0.1:6543/amounts/sayanS');
        const json2 = await response.json();
        console.log("fetchapi func json: "+JSON.stringify(json2.stonks));
        this.setState({
            wex: json2
        })
        return json2;
    };

    feedAPI()
    {
        var x = "empty";
        let t =  Promise.resolve(hit_api_feed('http://127.0.0.1:6543/userfeed/'));
        t.then(
            (data) => {
                console.log(data);
                this.setState({
                    feed: data.feed
                })
            });
    }


    /**
     * react notes: adding a key to the children you want to re-render will make them re-render every time.
     * no need for any componentWillReceiveProps() or anything like that
     *
     * TODO: add cors support for API
     * @returns {*}
     */

    render() {
        var appUN = this.state.message[1];
        console.log("app un " + appUN);
        console.log("message " + this.state.message);
        console.log(this.state.wex);
        const { selected } = this.state;
        // Create menu from items
        console.log(this.state.feed);
        var t = this.fetchapi();
        const menu = this.menuItems;
        return (
            <div className="App">
                <div>{this.state.feed}</div>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
                <Jumbotron fluid>

                    <Container>
                        <Typography variant={"h3"}>Assets Manager</Typography>
                        <h1>{appUN ? appUN + ' your portfolio is: ' : ''}</h1>
                        <p>

                        </p>
                    </Container>
                    <Button variant={"contained"} color={"primary"} onClick={() => this.setRedirect("vendors")}>Vendors</Button>
                    <Button variant={"contained"} color={"primary"} onClick={() => this.setRedirect("graphs")}>Graphs</Button>
                    <Button variant={"contained"} color={"primary"} onClick={() => this.setRedirect("realestate")}>Real Estate</Button>
                    <Button variant={"contained"} color={"primary"} onClick={() => this.feedAPI()}> fuck js </Button>
                    <Button variant={"contained"} color={"primary"} onClick={() => this.fetchapi()}> fuck js even more</Button>

                </Jumbotron>
                <LoginField onSubmitMessage = {this.onSubmitMessage}/>
                <ScrollMenu
                    data={menu}
                    arrowLeft={ArrowLeft}
                    arrowRight={ArrowRight}
                    selected={selected}
                    onSelect={this.onSelect}
                />
                <br/>

                {appUN ? <BasicAccordion assetValue={'800'}/> : ''}

                <br/>
                {appUN ? <OwnedStocksTable key={this.state.message[1]+"2"} un={this.state.message[1]}/> : ''}
                {this.renderRedirect()}


            </div>
        );
    }
}

export default App;
