import React from "react";
import {InputGroup} from "react-bootstrap";
import {FormControl} from "react-bootstrap";
import {Form} from 'react-bootstrap'
//import ReactBootstrapStyle from '@bit/react-bootstrap.react-bootstrap.internal.style-links';
import {Button} from "react-bootstrap";
import {TextField} from "@material-ui/core";

class LoginField extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            res: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);   //bind to handleSubmit
        //this.onSubmit = this.onSubmit.bind(this);
        this.onSubmit = this.onSubmit.bind(this);       //bind to onSubmit
    }

    handleSubmit(event) {
        console.log("button pressed");
        event.preventDefault();
        const data = new FormData(event.target);

        fetch('http://127.0.0.1:6543/bigpost/', {
            method: 'POST',
            body: data,
            res: []
        })
            .then(response => response.json())
            .then(
                (response) => {
                    this.setState({
                        res: response.bigyoungmoney
                    });
                    console.log(response);
                    console.log(this.state.res[0][1]);      //bs doubly indexed array? ["key", "value"] must be indexed twice
                    this.onSubmit();        //called to pass props to parent => App in this case
                }
            );
        //this.onSubmit();

    }

    // called in handleSubmit to pass the data to the parent app.
    onSubmit() {
        //console.log(this.state);
        this.props.onSubmitMessage(this.state.res[0]);
    }


    render() {
        return(
            <form onSubmit={this.handleSubmit}>
                <InputGroup className="mb-3">
                    <FormControl
                        placeholder="Recipient's username"
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                        id="filled-basic" name="username"  label={'Username'} type="text"
                    />
                    <InputGroup.Append>

                        <Button type="submit" className="button-submit" variant="outline-secondary">Submit</Button>

                    </InputGroup.Append>
                </InputGroup>
            </form>

        );
    }
}

export default LoginField;