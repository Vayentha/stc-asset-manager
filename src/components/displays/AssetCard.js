import React from "react";
import {Card} from "react-bootstrap";
import {Button} from "react-bootstrap";

class AssetCard extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            cards: []
        }
    }

    render() {
        return(
                <Card style={{ width: '18rem', justifyContent: 'center' }}>
                    <Card.Body>
                        <Card.Title>Assets for Sale</Card.Title>
                        <Card.Text>
                            Place asset cards here desu
                        </Card.Text>
                        <Button variant="primary">Buy Asset</Button>
                    </Card.Body>
                </Card>
    );
    }
}

export default AssetCard;