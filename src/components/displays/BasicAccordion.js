import React from "react";
import {Accordion} from "react-bootstrap";
import {Card} from "react-bootstrap";
import {Button} from "react-bootstrap";


class BasicAccordion extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            contentPane: 0


        }
    }

    render() {
        return(
            <div>
                <Accordion defaultActiveKey="0">
                    <Card>
                        <Card.Header>
                            <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                Your Assets
                            </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="0">
                            <Card.Body>Assets Value: {this.props.assetValue}</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Card.Header>
                            <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                Bad Stocks
                            </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="1">
                            <Card.Body>Bad Stocks</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </div>
        );
    }
}

export default BasicAccordion;