import React, {useState} from "react";
import ListItem from "@material-ui/core/ListItem";
import {ListItemText} from "@material-ui/core";
import List from "@material-ui/core/List";

class StockTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            uname: props.un,
            items: []
        };
    }

    componentDidMount() {
        var sreq = `http://127.0.0.1:6543/fetch/${this.state.uname}`;
        fetch(sreq)
            .then(result => result.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.stocksOwned             /* NOTE: to get this to work, you need to index the JSON values here */
                    });                                             /* as well as have CORS set up so that the request doesn't fail.     */
                },                                                  /* i am indexing the stocksOwned JSON (primary key stocksOwned) here */
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error)
        {
            return <div>Error: {error.message}</div>;
        }
        else if (!isLoaded)
        {
            return <div>Loading...</div>;
        }
        else
        {
            return (
               <List>
                    {items.map(item => (
                        <ListItem button key={item.toString()}>
                           <ListItemText key={item.name} primary={ `${item}`}/>
                        </ListItem>
                    ))}
               </List>
            );
        }
    }
}

export default StockTable;