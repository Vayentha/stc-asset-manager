import React from "react";
import Button from "@material-ui/core/Button";


class HeartButton extends React.Component {
    state = {
        likes: 0
    };

    addLike = () => {
        let newCount = this.state.likes + 1;
        this.setState({
            likes: newCount
        });
    };

    render() {
        const likes = this.state.likes;
        if (likes === 0) {
            return (
                    <Button variant={"contained"} color={"primary"}
                        className="button"
                        onClick={this.addLike}
                    >   CLICK ME
                        <i className="far fa-heart fa-lg" style={{ color: "#33c3f0" }}></i>
                    </Button>
            );
        }
        if (likes === 1) {
            return (
                    <Button variant={"contained"}  color={"primary"} className={"button"}  onClick={this.addLike}> CLICK ME
                        <i className="fas fa-heart fa-lg" style={{ color: "red" }}></i>
                    </Button>
            );
        }
        if (likes > 1) {
            return (
                    <Button  variant={"contained"}  color={"primary"} className="button" onClick={this.addLike}>
                        <i className="fas fa-heart fa-lg" style={{ color: "red" }}></i>{" "}
                          CLICK ME  {likes}
                    </Button>
            );
        }
    }
}

export default HeartButton;