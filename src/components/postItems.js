import React from "react";
import {TextField} from "@material-ui/core";
import {Button} from "@material-ui/core";


class MyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            res: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);   //bind to handleSubmit
        //this.onSubmit = this.onSubmit.bind(this);
        this.onSubmit = this.onSubmit.bind(this);       //bind to onSubmit
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        fetch('http://127.0.0.1:6543/bigpost/', {
            method: 'POST',
            body: data,
            res: []
        })
            .then(response => response.json())
            .then(
                (response) => {
                    this.setState({
                       res: response.bigyoungmoney
                    });
                    console.log(response);
                    console.log(this.state.res[0][1]);      //bs doubly indexed array? ["key", "value"] must be indexed twice
                    this.onSubmit();        //called to pass props to parent => App in this case
                }
            );
            //this.onSubmit();

    }

    // called in handleSubmit to pass the data to the parent app.
    onSubmit() {
        //console.log(this.state);
        this.props.onSubmitMessage(this.state.res[0]);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div>
                    <TextField id="filled-basic" name="username"  label={'Username'} type="text" />
                    <Button type="submit" label="login" className="button-submit" primary={true}> log in </Button>
                </div>
            </form>
        );
    }
}

export default MyForm;