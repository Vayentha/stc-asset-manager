import React from "react";
import Box from "@material-ui/core/Box";

class DevBanner extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            host: props.host,
            dbhost: props.dbhost,
            gport: props.gport
        }
    }

    render() {
        return(
            <Box component="span" display="inline" p={1} m={1} bgcolor={"background.paper"}> web host: {this.state.host}:{this.state.port}  database:{this.state.dbhost}</Box>
        );
    }
}

export default DevBanner;