import React from "react";
import ListItem from "@material-ui/core/ListItem";
import {ListItemText} from "@material-ui/core";
import List from "@material-ui/core/List";
import {Popover} from "@material-ui/core";
import {Button} from "@material-ui/core";


/***
 *  OwnedStocksTable() ==> generates a list of owned stocks from
 *  the api call GET/amounts/username.
 *
 *  userName is passed through the props.
 *
 ***
 */
class OwnedStocksTable extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            uname: props.un,
            items: []
        };
    }




    componentDidMount()
    {
        console.log("ownedstocks un: "+ this.state.uname);
        var sr = `http://127.0.0.1:6543/amounts/${this.state.uname}`;
        fetch(sr)
            .then(result => result.json())
            .then(
                (result) => {
                    /*  put more trash js here  */
                    this.setState({
                       isLoaded: true,
                       items: result.stonks
                    });

                },

                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }


    render() {
        const { error, isLoaded, items } = this.state;
        if (error)
        {
            return <div>Error: {error.message}</div>;
        }
        else if (!isLoaded)
        {
            return <div>Loading...</div>;
        }
        else
        {
            return (
                <List>
                    {items.map(item => (
                        <ListItem key={item.stockTicker.toString()}>

                            <Button variant={"contained"}><ListItemText primary={ ` ${item.stockTicker}  ` }/></Button>
                            &nbsp;&nbsp;&nbsp;<ListItemText primary={ ` ${item.stockName}` }/>
                            <Button><ListItemText primary={`$${item.stockPrice}`}/></Button>
                        </ListItem>
                    ))}
                </List>
            );
        }
    }
}
export default OwnedStocksTable;