import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import App from "./App";
import Vendors from "./Vendors";
import Graphs from "./Graphs";
import RealEstate from "./RealEstate";

/***
 *  TODO: add al routes to app
 *
 *
 *
 *
 */

export default class Routes extends Component {
    render() {
        return (
                <Switch>
                    <Route path="/" exact component={App} />
                    <Route path="/vendors" component={Vendors}/>
                    <Route path='/graphs' component={Graphs}/>
                    <Route path="/realestate" component={RealEstate}/>
                </Switch>
        )
    }
}

